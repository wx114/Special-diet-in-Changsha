# 长沙特色饮食采集

#### 介绍
数据来源于慧游长沙文旅平台+美团，采集长沙热门特色饮食信息。

#### 需求分析
1. 采集长沙美食相关信息。
2. 数据来源于慧游长沙文旅平台（小程序）+美食天下（Web）。
![img.png](data/img.png)
![img.png](data/img1.png)
#### 安装教程

1.  pip install httpx
2.  pip install parsel

#### 使用说明

1.  python 执行采集.py
