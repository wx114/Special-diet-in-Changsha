import 采集长沙美食列表 as crawler_list
import 采集饮食详情信息 as crawler_detail

import 采集美食天下湘菜列表 as food_world_list
import 采集美食天下湘菜详情 as food_world_detail

if __name__ == '__main__':
    # 采集长沙文旅小程序美食数据
    # with open('../data/data1.json', 'a+', encoding='utf-8') as f:
    #     f.write('[\n')
    # for diet_url in crawler_list.main():
    #     crawler_detail.main(diet_url)

    # 采集美食天下加大众点评Web美食数据
    for name in food_world_list.main():
        food_world_detail.main(name)

    with open('../data/data1.json', 'a+', encoding='utf-8') as f:
        f.write(']')
